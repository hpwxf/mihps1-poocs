#include <iostream>

class Interface {
public:
  virtual void init() = 0;
  virtual int compute() = 0;
  virtual void constant() const = 0;
  virtual ~Interface() = default;
};

class Implementation : public Interface {
public:
  Implementation(int i) : m_i(i) { std::cout << "Ctor of " << this << std::endl; }
  Implementation() = delete;
  Implementation(const Implementation & a) { std::cout << "Copy of " << &a << " -> " << this << std::endl; }
  ~Implementation() { std::cout << "Dtor of " << this << std::endl; }

public:
  void init() override { std::cout << "Ctor of " << this << std::endl; }
  int compute() override { return m_i; }
  void constant() const final { std::cout << "Implementation::constant" << std::endl; }
private:
  int m_i;
};

class OverImplementation : public Implementation {
public:
  // void constant() const final { std::cout << "OverImplementation::constant" << std::endl; }
};

void f(Interface * a) {
  a->compute();
}

void g(const Interface & a) {
  a.constant();
}

void h(Implementation a) {
  a.constant();
}

int main() {
  {
    std::cout << "First part\n";
    Interface * i = new Implementation(2);
    f(i); 
    g(*i); 
    delete i;
  }
  
  {
    std::cout << "Second part\n";
    Implementation a(3);
    Implementation b = a;
    Implementation c = a;
    h(c);
  }
}
