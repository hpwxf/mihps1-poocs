int main() {
  auto f1 = [](int i)->int { return i*i; };

  auto g = [f=f1](int i)->int {
    auto f1 = [](int i)->int { return i*i; };
    return f(i) * f1(i);
  };
  
  g(2);
  f1(2);
 
  {
    int f = 3;
  }

  // f; "was not declared in this scope"
  
}
