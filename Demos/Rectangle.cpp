#include <iostream>

class Rectangle {
public:
	Rectangle(int largeur, int hauteur) : m_largeur(largeur), p(this) { this->m_hauteur = hauteur; }
	// Rectangle(Point2D haut_gauche, Point2D bas_droit) { /* ... */ }
	Rectangle(const Rectangle & r) : p(this) {
		this->m_largeur = r.m_largeur;
		this->m_hauteur = r.m_hauteur;
	}
	void operator=(const Rectangle &) = delete;
private:
	int m_largeur;
	int m_hauteur;
	const Rectangle * const p;
public:
	void affiche() { std::cout << "Rectangle " << this << " : " << p << " : " << m_largeur << " x " << m_hauteur << std::endl; }
};

int main() {
	// Rectangle r0; // illegal car non d�fini
	Rectangle r1(2,3);
	Rectangle r2{2,3}; // n'est pas une initialisation 'champs-�-champs' 
                           // comme pour les structures simples : appel le m�me constructeur que r1
	r1.affiche();
	r2.affiche();
	
	Rectangle r3 = r1;
        r3.affiche();
	// r3 = r1; // illegal si operator=(const Rectangle &) = delete;
}
