#include <iostream>

int main() {
  auto f = [](int i) -> int { 
    std::cout << "i=" << i << std::endl;
    return i;
  };
  
  int y = f(1);
  std::cout << "f(1)=" << y << std::endl;
}
