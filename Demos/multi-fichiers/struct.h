// L'oubli du mécanisme de "guard" provoquera une erreur en cas de double inclusion de ce fichier
//#ifndef STRUCT_H
//#define STRUCT_H

struct Structure { double x, y; };

//#endif /* STRUCT_H */
