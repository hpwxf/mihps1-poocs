#include <iostream>
#include <cmath>

struct Point3D { double x,y,z; };

std::ostream & operator<<(std::ostream & o, const Point3D & p) {
  return o << "(" << p.x << ", " << p.y << ", " << p.z << ")";
}

Point3D operator+(const Point3D & a, const Point3D & b) { return { a.x + b.x, a.y + b.y, a.z + b.z }; }

auto norm1(const Point3D & p)->double { return std::abs(p.x) + std::abs(p.y) + std::abs(p.z); }

auto eval = [](auto norm, const Point3D & p)->double { return norm(p); };

int main() {
  
  auto norm2 = [](const Point3D & p)->double { return std::sqrt(p.x*p.x+p.y*p.y+p.z*p.z); };

  Point3D a{1,2,3};
  Point3D b = {3,4,5};
  Point3D c = a + b + b;
  std::cout << a << " + 2*" << b << " = " << c << std::endl;
  std::cout << "norm2(c) = " << norm2(c) << std::endl;
  std::cout << "eval(norm1,c) = " << eval(norm1,c) << std::endl;
  std::cout << "eval(norm2,c) = " << eval(norm2,c) << std::endl;
  return 0;
}
