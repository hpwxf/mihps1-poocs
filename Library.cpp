#include <string>    // pour std::string
#include <vector>    // pour std::vector
#include <iostream>  // pour std::cout & cie
#include <exception> // pour std::exception

namespace Library { // Le tout dans un namespace

  typedef size_t ReferenceType; // Assimilable � un identifiant, code barre sur document ou carte de membre
  class Library; // pr�-d�claration pour pouvoir l'utiliser dans MemberAccount

  /*********************************************************************************
   ********************           Classes d'interface                ***************
   *********************************************************************************/

  class IDocument {
  public:
    enum Type { eBook, eNewsPaper, eComicBook, typeCount }; // en attendant une gestio dynamique des types
  public:
    virtual ~IDocument() { }
  public:
    virtual void print(std::ostream & o) const = 0;
    virtual Type type() const = 0;
  };

  class IVolume : public IDocument {
  public:
    virtual ~IVolume() { }
  public:
    virtual std::string author() const = 0;
  };

  class IMemberPolicy 
  {
  public:
    virtual ~IMemberPolicy() { }
  public:
    virtual bool canBorrow(const IDocument * document, const std::vector<const IDocument *> & documents) const = 0;
  };

  /*********************************************************************************
   ********************             Classes techniques               ***************
   *********************************************************************************/

  class Exception {
  public:
    Exception(const std::string message) : m_message(message) { }
    virtual ~Exception() { }
  public:
    const std::string & message() const { return m_message; }
  private:
    const std::string m_message;
  };

  /*********************************************************************************
   ********************             Classes principales              ***************
   *********************************************************************************/

  class MemberAccount {
  public:
    MemberAccount(const std::string & name, const IMemberPolicy * policy, Library & library, ReferenceType ref) 
      : m_name(name), m_policy(policy), m_library(library), m_reference(ref) { }
    virtual ~MemberAccount() { }

  public: // accesseurs
    std::string name() const { return m_name; }
    const IMemberPolicy * policy() const { return m_policy; }

  public: // m�thodes techniques
    void print(std::ostream & o) const { o << "Membre:" << m_name; }
    void borrow(ReferenceType ref) throw (Exception);

  private:
    const std::string m_name;
    const IMemberPolicy * m_policy;
    Library & m_library;
    const ReferenceType m_reference;
  };

  class Library {
  public:
    Library(const std::string name) : m_name(name) { };
    virtual ~Library();
  private:
    // Protection contre les copies 'involontaires'
    Library(const Library &);
    void operator=(const Library &);

  public:
    //! @name Accesseurs
    const std::string & name() const { return m_name; }
    MemberAccount * getMemberAccount(ReferenceType ref);
    const IDocument * getDocument(ReferenceType ref) const;
    std::vector<const IDocument *> getBorrowings(const MemberAccount & member) const;

  public:
    void print(std::ostream & o) const;
  
  public:
    ReferenceType addDocument(IDocument * document);
    ReferenceType addMember(const std::string & name, const IMemberPolicy * policy);
    void borrow(ReferenceType ref, MemberAccount & account) throw (Exception);    

  private:
    const std::string m_name;
    std::vector<MemberAccount*> m_members;
    std::vector<IDocument*> m_documents;

    typedef std::vector<std::pair<const MemberAccount*,const IDocument*> > Borrowings;
    Borrowings m_borrowings;
  };

  std::ostream & operator<<(std::ostream & o, const Library & library) 
  { 
    library.print(o); 
    return o; 
  }

  /*********************************************************************************
   ********************        Classes  d'impl�mentation             ***************
   *********************************************************************************/

  class Book : public IVolume {
  public:
    Book(const std::string & title, const std::string & author) 
      : m_title(title), m_author(author) { }
    virtual ~Book() { }

  public:
    void print(std::ostream & o) const { o << "Titre:'" << m_title << "', Auteur:'" << m_author << "'"; }

  public:
    IDocument::Type type() const { return eBook; }
    std::string author() const { return m_author; }

  private:
    const std::string m_title;
    const std::string m_author;
  };

  class NewsPaper : public IDocument {
  public:
    NewsPaper(const std::string & journal, const std::string & date)
      : m_journal(journal), m_date(date) { }
    virtual ~NewsPaper() { }

  public:
    void print(std::ostream & o) const { o << "Journal:'" << m_journal << "', Date:'" << m_date << "'"; }

  public:
    IDocument::Type type() const { return eNewsPaper; }

  private:
    const std::string m_journal;
    const std::string m_date;
  };

  class ComicBook : public IVolume {
  public:
    ComicBook(const std::string & title, const std::string & collection, const std::string & author)
      : m_title(title), m_collection(collection), m_author(author) { }
    virtual ~ComicBook() { }

  public:
    void print(std::ostream & o) const { o << "Titre:'" << m_title << "' Collection:'" << m_collection << "', Auteur:'" << m_author << "'"; }

  public:
    std::string author() const { return m_author; }
    IDocument::Type type() const { return eComicBook; }

  private:
    const std::string m_title;
    const std::string m_collection;
    const std::string m_author;
  };

  class TeacherPolicy : public IMemberPolicy {
  public:
    virtual ~TeacherPolicy() { }
  public:
    bool canBorrow(const IDocument * document, const std::vector<const IDocument *> & documents) const;
  };
  
  class StudentPolicy : public IMemberPolicy {
  public:
    virtual ~StudentPolicy() { }
  public:  
    bool canBorrow(const IDocument * document, const std::vector<const IDocument *> & documents) const;
  };

  /*********************************************************************************
   ********************               Impl�mentation                 ***************
   *********************************************************************************/

  Library::
  ~Library()
  {
    for(size_t i=0;i<m_documents.size();++i) { delete m_documents[i]; m_documents[i] = NULL; }
    for(size_t i=0;i<m_members.size();++i) { delete m_members[i]; m_members[i] = NULL; }
  }

  ReferenceType
  Library::
  addDocument(IDocument * document)
  {
    int ref = m_documents.size();
    //   document->registerIn(*this,ref);
    m_documents.push_back(document);
    return ref;
  }

  ReferenceType Library::
  addMember(const std::string & name, const IMemberPolicy * policy)
  {
    // Pourrait aussi v�rifier les doublons dans la base de donn�es
    int ref = m_members.size();
    m_members.push_back(new MemberAccount(name, policy, *this, ref));
    return ref;
  }

  void
  Library::
  print(std::ostream & o) const
  {
    o << "Les documents:\n";
    for(size_t i=0;i<m_documents.size();++i) 
      { 
	o << "Ref " << i << " : ";
	m_documents[i]->print(o);
	o << "\n";
      }
    o << "Les membres:\n";
    for(size_t i=0;i<m_members.size();++i) 
      { 
	o << "Ref " << i << " : ";
	m_members[i]->print(o);
	o << "\n";
	std::vector<const IDocument *> documents = getBorrowings(*m_members[i]);
	if (!documents.empty())
	  {
	    o << "\ta emprunt�:\n";
	    for(size_t i=0;i<documents.size();++i) { o << "\t\t"; documents[i]->print(o); o << "\n"; }
	  }
      }
    o << "\n";
  }

  MemberAccount * 
  Library::
  getMemberAccount(ReferenceType ref)
  {
    if (ref >= 0 && ref < m_members.size())
      return m_members[ref];
    else
      return NULL;
  }

  const IDocument * 
  Library::
  getDocument(ReferenceType ref) const
  {
    if (ref >= 0 && ref < m_documents.size())
      return m_documents[ref];
    else
      return NULL;
  }

  std::vector<const IDocument *>
  Library::
  getBorrowings(const MemberAccount & account) const
  {
    std::vector<const IDocument *> borrowings;
    for(Borrowings::const_iterator i=m_borrowings.begin(); i!=m_borrowings.end(); ++i) 
      {
	if (i->first == &account)
	  borrowings.push_back(i->second);
      }
    return borrowings;
  }

  void 
  MemberAccount::
  borrow(ReferenceType ref) throw (Exception)
  {
    m_library.borrow(ref, *this);
  }

  void 
  Library::
  borrow(ReferenceType ref, MemberAccount & account) throw (Exception)
  {
    const IDocument * document = getDocument(ref);
    if (!document) throw Exception("Document inconnu");
    
    std::vector<const IDocument *> documents = getBorrowings(account);
    const bool can_borrow = account.policy()->canBorrow(document, documents);

    if (can_borrow)
      { // Gestion de l'emprunt
	std::pair<const MemberAccount *, const IDocument *> b(&account, document);
	m_borrowings.push_back(b);
	std::cout << "Emprunt du document Ref=" << ref << " accept�\n";
      }
    else
      {
        throw Exception("Emprunt non autoris�");
      }
  }
  
  bool
  StudentPolicy::
  canBorrow(const IDocument * document, const std::vector<const IDocument *> & documents) const 
  {
    // On aurait pu aussi penser que la politique parcours tous les documents emprunt�s (de tous les membres)
    // et d�cide apr�s ce parcours, de ce qui est autoris� (mais ceci aurait impliqu� de nombreux appels virtuels...)

    static const size_t type_count = IDocument::typeCount;
    size_t counts[type_count];
    for(size_t i=0;i<type_count;++i) counts[i] = 0;
    for(size_t i=0;i<documents.size();++i) counts[documents[i]->type()]++;
    for(size_t i=0;i<documents.size();++i) if (documents[i] == document) return false;

    if (document->type() == IDocument::eBook || 
	document->type() == IDocument::eComicBook) // ici on autorise aussi l'emprunt des BDs
      return (counts[IDocument::eComicBook]+counts[IDocument::eBook] < 2);
    else 
      return false; // mais rien d'autres
  }

  bool
  TeacherPolicy::
  canBorrow(const IDocument * document, const std::vector<const IDocument *> & documents) const 
  {
    static const size_t type_count = IDocument::typeCount;
    size_t counts[type_count];
    for(size_t i=0;i<type_count;++i) counts[i] = 0;
    for(size_t i=0;i<documents.size();++i) counts[documents[i]->type()]++;
    for(size_t i=0;i<documents.size();++i) if (documents[i] == document) return false;
    
    if (document->type() == IDocument::eBook)
      return (counts[IDocument::eBook] < 10);
    else if (document->type() == IDocument::eNewsPaper)
      return (counts[IDocument::eNewsPaper] < 5);
    else
      return false;
  }
} // fin du namespace Library

/*********************************************************************************
 ********************            Programme principal               ***************
 *********************************************************************************/

int main() {
  Library::Library myLibrary("POOCS Library");
  const Library::ReferenceType refDoc1 = myLibrary.addDocument(new Library::Book("Titre","Auteur"));
  const Library::ReferenceType refDoc2 = myLibrary.addDocument(new Library::ComicBook("Titre de la BD","Collection","Auteur"));
  const Library::ReferenceType refDoc3 = myLibrary.addDocument(new Library::NewsPaper("Titre du journal","Date"));

  Library::StudentPolicy studentPolicy;
  Library::TeacherPolicy teacherPolicy;

  const Library::ReferenceType refMember1 = myLibrary.addMember("Nom1", &studentPolicy);
  const Library::ReferenceType refMember2 = myLibrary.addMember("Nom2", &studentPolicy);
  const Library::ReferenceType refMember3 = myLibrary.addMember("Nom3", &teacherPolicy);

  std::cout << "Ma biblioth�que: '" << myLibrary.name() << "'\n" << myLibrary << std::endl;

  try 
    {
      Library::MemberAccount * account = myLibrary.getMemberAccount(refMember2); // ne teste pas le retour nul!
      account->borrow(refDoc2);
      account->borrow(refDoc3);
    } catch(Library::Exception & e)
    {
      std::cerr << "Emprunt impossible : " << e.message() << std::endl;
    }

  std::cout << "\nMa Biblioth�que: '" << myLibrary.name() << "' content:\n" << myLibrary << std::endl;
};

/* Evolutions possibles:
 * - On pourrait compl�ter les retours n�gatifs de IMemberPolicy::canBorrow avec une exception plus explicite
 * - Transformer les r�f�rences enti�res aux IDocument's et aux IMember's en types
 * - Utiliser un pattern 'Factory' pour la cr�ation des objets IDocument's (sur les membres Library le fait d�j�)
 *   et de m�me sur toutes les 'entit�s factorisables' (assimilables � une indexation au sens base de donn�es)
 * - On peut aussi inveser la gestion de la politique d'emprunt en la faisant porter par les documents au lieu des membres
 * - Rendre dynamique le type des documents (IDocumentType + 'd�corations')
 * - l'�num�ration des emprunts 'Library::borrowings' peut �tre plus 'efficace' via un it�rateur sp�cialis� 
 *   et/ou des pr�-calculs adapt�s aux besoins des politiques (mais cela fige un peu les possibilit�s de IMemberPolicy)
 * - Ajouter des m�thodes de recherche � partir de r�f�rences
 * - NB: le flow de traitement de l'emprunt n'est pas thread-safe
 */
