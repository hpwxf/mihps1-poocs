Exemple de commande gnuplot.

1- lancer la commande 'gnuplot', l'invite de commande gnuplot apparaît: 
gnuplot>

2- pour tracer une coubre dans des fichiers 'fichier*.txt', y taper:
gnuplot> plot 'fichier1.txt' with lines, 'fichier2.txt' w l

3- pour sortir, taper 'exit'
gnuplot> exit

