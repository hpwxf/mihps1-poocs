/*
 * DReal.cpp
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#include "DReal.h"
#include <cmath>

std::ostream & operator<<(std::ostream & o, const DReal & r) { return o << "(" << r.value << "," << r.dvalue << ")"; }

DReal x_at(const Real v) { return DReal{v,1}; }
DReal operator+(const DReal & f, const DReal & g) { return {f.value+g.value, f.dvalue+g.dvalue}; }
DReal operator-(const DReal & f, const DReal & g) { return {f.value-g.value, f.dvalue-g.dvalue}; }
DReal operator*(const DReal & f, const DReal & g) { return {f.value*g.value, g.value*f.dvalue+f.value*g.dvalue}; }

DReal operator/(const DReal & f, const DReal & g) throw (MyException) {
  if (g.value == 0.) {
    throw MyException{"Null denominator"};
  }
  return {f.value/g.value,
	 (f.dvalue*g.value-g.dvalue*f.value)/(g.value*g.value)};
}

DReal sin(const DReal & g) {
	return {sin(g.value), g.dvalue*cos(g.value)};
}

DReal sqrt(const DReal & g) {
  if (g.value <= 0) {
    throw MyException{"Negative radical"};
  }
  return DReal{sqrt(g.value), .5*g.dvalue}/DReal{sqrt(g.value)};
}

