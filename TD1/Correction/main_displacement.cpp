/*
 * main_displacement.cpp
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#include <iostream>
#include <cmath>
#include "DReal.h"
#include "Integrate.h"
#include "Plot.h"
#include "Newton.h"

int main() {
	const Real Pi = 4*std::atan(1);

    auto altitude = [](DReal x)->DReal { return sin(sqr(x/100))*x/10; }; // altitude en fonction de la position
    //  auto altitude = [](DReal x)->DReal { return sin((x)); }; // altitude en fonction de la position
    //  auto altitude = [](DReal x)->DReal { return x; }; // altitude en fonction de la position
    //	auto altitude = [](DReal x)->DReal { return x*x; }; // altitude en fonction de la position
	auto pente = [altitude](Real x)->Real { return std::atan(altitude(x_at(x)).dvalue); };
	const Real xmin = 0;
	const Real xmax = 300;

	const Real puissance = 10000; // _W
	const Real vmax = 10; // _m/s
	const Real masse = 100; // _kg
	const Real gravite = 10; // _m/s^2
	const Real poids = masse * gravite; // _N

#if 1
	// Modéle de frottement sans effet significatif de l'air
	const Real frottement = puissance/vmax; // _N
	auto vitesse = [puissance, frottement, poids](Real angle)->Real { return puissance/(frottement + poids * sin(angle)); };
#else
	// Modèle avec effet significatif avec l'air
	auto frottement = [](DReal v)->DReal { return 1000 + 0.8 * v * v; };
	auto vitesse = [puissance, frottement, poids, vmax](Real angle)->Real {
		// ATTN : minimums locaux possibles suivant la fonction de frottement (par exemple si coef = 225)
		auto f = [puissance, frottement, poids, vmax, angle](DReal x)->DReal { return puissance - x*(frottement(x) + poids * sin(angle)); };
		const Real vguest = puissance/(frottement(0).value + poids * sin(angle));
		const Real v = newton(vguest, 1e-9, f);
		return v;
	};
#endif

	plot("altitude.gp",xmin,xmax,100,altitude);
	plotd("pente.gp",xmin,xmax,100,pente);
        plotd("vitesse.gp",xmin,xmax,100,[pente,vitesse](Real x)->Real { return vitesse(pente(x)); });

	std::cout << "Vitesse sur le plan: " << vitesse(0) << "_m/s\n";
	std::cout << "Vitesse sur une montée à 10°: " << vitesse(10*Pi/180) << "_m/s\n";
	std::cout << "Vitesse sur une descente à 10°: " << vitesse(-10*Pi/180) << "_m/s\n";
        auto dh = [altitude](Real x)->Real { return altitude(x_at(x)).dvalue; };
        auto sqr = [](Real x)->Real { return x*x; };

	const Real duree = integrate(xmin,xmax,1000, [pente,vitesse,dh,sqr](Real x)->Real { return std::sqrt(sqr(dh(x))+1)/vitesse(pente(x)); });
	std::cout << "Temps de parcours: " << duree << "\n";

	return 0;
}

