/*
 * Newton.h
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#ifndef NEWTON_H
#define NEWTON_H

#include "DReal.h"
#include "MyException.h"

auto newton = [](Real x0, const Real eps, auto f)->Real { // C++14
	try {
		for(int i=0;i<100;++i) {
			DReal fx = f(x_at(x0));
			// std::cout << "x0:" << x0 << " f(x0):" << fx << "\n";
			Real x1 = x0 - fx.value / fx.dvalue;
			if (std::abs(x1-x0)<eps) return x1;
			x0 = x1;
		}
		throw MyException{"Newton does not converge"};
	} catch (MyException & e) {
		std::cout << "Exception catched while solving newton:" << e.message << "\n";
		throw;
	}
};

#endif /* NEWTON_H */
