cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)
project (TD1 CXX)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# CMake Tutorial: 
# https://cmake.org/cmake-tutorial/

include_directories ("${PROJECT_SOURCE_DIR}")

# add the executable
add_executable(differentiation main_differentiation.cpp DReal.cpp)
add_executable(integration main_integration.cpp DReal.cpp)
add_executable(displacement main_displacement.cpp DReal.cpp)
