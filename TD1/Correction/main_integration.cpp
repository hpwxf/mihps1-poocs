/*
 * main_integration.cpp
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#include <iostream>
#include <cmath>
#include "DReal.h"
#include "Integrate.h"

int main() {
	auto f = [](Real x)->Real {return x*x;};
	const Real xmin = 0;
	const Real xmax = 2;
	const Real solution_numerique = integrate(xmin,xmax,100,f);
	const Real solution_analytique = (xmax*xmax*xmax-xmin*xmin*xmin)/3;

	std::cout << "Integration sur [0:2] de x->x^2: " << solution_numerique << "; erreur:" << std::abs(solution_analytique-solution_numerique) << "\n";

	return 0;
}


