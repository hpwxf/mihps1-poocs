/*
 * integrate.h
 *
 *  Created on: 16 oct. 2016
 *      Author: Pascal
 */

#ifndef INTEGRATE_H
#define INTEGRATE_H

#include "MyException.h"

auto integrate = [](const Real xmin, const Real xmax, const int n, auto f)->Real { // C++14
  try {
	  const Real dx = (xmax-xmin)/n;
	  const Real fmin = f(xmin);
	  const Real fmax = f(xmax);
	  Real fmidcumul = 0.;
	  for(int i=1;i<n;++i) { fmidcumul += f(dx*i); }
	  return dx*((fmin+fmax)/2+fmidcumul);
  } catch (MyException & e)
  {
	  std::cout << "Exception catched while evaluating integration\n";
	  throw;
  }
};

#endif /* INTEGRATE_H */
