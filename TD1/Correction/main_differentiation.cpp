/* - typedef
   - I/O
   - initalization
   - operator
   - overload
   - exception 
   - C++14 lambda (auto)
   
   Gnuplot avec "set terminal dumb" pour le mode Ascii Art
*/

#include <iostream>
#include <cmath>
#include "DReal.h"
#include "Plot.h"

int main() {

  Real Pi = 4*atan(1);
  std::cout << "Pi is " << DReal{Pi} << std::endl;

  Real x0 = Pi/2;
  try {
    DReal v = cube(sin(x_at(x0)));
    std::cout << "sin(x^3) at " << x0 << " : " << v << "\n";
  } catch (MyException & e) {
    std::cout << "Exception catched while evaluating sin(x)^3 at " << x0 << "\n";
  }
  
  Real x1 = 1;
  try {
    DReal v = 1/cube(x_at(x1));
    std::cout << "1/x^3 at " << x1 << " : " << v << "\n";
  } catch (MyException & e) {
    std::cout << "Exception catched while evaluating 1/x^3 at " << x1 << "\n";
  }

  auto sin3 = [](DReal x)->DReal { return cube(sin(x+1)); };
  auto inv = [](DReal x)->DReal { return 1./x; };

  plot("sin3.gp",0,Pi,50,sin3);
  // plot("f.gp",0,1,20,[](DReal x)->DReal { return inv(x); });
  plot("f.gp",0,1,100,[inv](DReal x)->DReal { return sin(inv(x)); });
  plot("inv.gp", -1, 1, 21, inv);

  return 0;
}
