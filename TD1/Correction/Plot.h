#ifndef PLOT_H
#define PLOT_H

#include "DReal.h"
#include <fstream>
#include <iostream>
#include <functional>
#include "MyException.h"

auto plot = [](const char * filename, const Real xmin, const Real xmax, const int n, auto f) { // C++14
  std::ofstream out(filename);
  Real dx = (xmax-xmin)/(n-1);
  Real x = xmin;
  for(int i=0;i<n;++i,x+=dx) {
    try {
      DReal fx = f(x_at(x));
      out << x << " " << fx.value << " " << fx.dvalue << "\n";
    } catch (MyException & e) {
      std::cout << "Exception " << e.message << " at position x=" << x << "\n";
    }
  }
};

auto plotd = [](const char * filename, const Real xmin, const Real xmax, const int n, std::function<Real(Real)> f) {
  std::ofstream out(filename);
  Real dx = (xmax-xmin)/(n-1);
  Real x = xmin;
  for(int i=0;i<n;++i,x+=dx) {
    try {
      Real fx = f(x);
      out << x << " " << fx << "\n";
    } catch (MyException & e) {
      std::cout << "Exception " << e.message << " at position x=" << x << "\n";
    }
  }
};

#endif /* PLOT_H */
